import { createApp } from 'vue';
import App from './App.vue';

import TreeDialog from './components/TreeDialog.vue';
import TreeItem from './components/TreeItem.vue';

const app = createApp(App);

app.component('tree-dialog', TreeDialog);
app.component('tree-item', TreeItem);

app.mount('#app');
